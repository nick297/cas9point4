from argparse import ArgumentParser, SUPPRESS
import seaborn as sns
import sys
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import pysam
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import pysamstats
sns.set_style('darkgrid')

# blast header names
names=['query',
    'subject',
    'id',
    'alignment length',
    'mismatches',
    'gap openings',
    'query start',
    'query end',
    'subject start',
    'subject end',
    'E value',
    'bit score']

## plotting
 
def accuracy(r,base):
    if r['ref'] == r[base]:
        return 0
    else:
        return 1

def mut_type(r,base):
    if r['ref'] == r[base]:
        return None
    else:
        return r['ref'] + r[base]

def loxPreadOr(r):
    if r['subject start'] < r['subject end']:
        return 'F'
    elif r['subject start'] > r['subject end']:
        return 'R'

def loxRefOrient(r):
    if r['flag']==0:
        if r['LoxP read orientation'] == 'F':
            return 'F'
        elif r['LoxP read orientation'] == 'R':
            return 'R'
    elif r['flag']==16:
        if r['LoxP read orientation'] == 'F':
            return 'R'
        elif r['LoxP read orientation'] == 'R':
            return 'F'

def hitStart(r):
    if r['LoxP ref orientation'] == 'F':
        return r['pos'] + r['query start']
    if r['LoxP ref orientation'] == 'R':
        return r['pos'] + r['query end']

def hitEnd(r):
    if r['LoxP ref orientation'] == 'F':
        return r['pos'] + r['query end']
    if r['LoxP ref orientation'] == 'R':
        return r['pos'] + r['query start']

def loadBlast(blast):
    df=pd.read_csv(blast,sep='\t',names=names)
    try:
        df['LoxP read orientation']=df.apply(loxPreadOr,axis=1)
    except:
        pass
    return df

def loadBam(ib):
    inBam=pysam.AlignmentFile(ib, "rb")
    for read in inBam.fetch():
        d={'query':read.query_name,
            'flag':read.flag,
            'ref':read.reference_name,
            'pos':read.reference_start,
            'mapQ':read.mapping_quality}
        yield d
    
#    df=pd.read_csv(sam,names=['query','flag','ref','pos','mapQ'])
#    return df

def mergeBamBlast(df,df2):
    df['LoxP read orientation']=df.apply(loxPreadOr,axis=1)
    df3=df2.merge(df,how='outer')
    df3['LoxP ref orientation']=df3.apply(loxRefOrient,axis=1)
    df3['Hit ref start']=df3.apply(hitStart,axis=1)
    df3['Hit ref end']=df3.apply(hitEnd,axis=1)

    df=df3[['subject','query','LoxP ref orientation']].groupby(['subject','LoxP ref orientation'])
    return df3

def preDFs(df,loxP_type):
    #df=df[df['loxP type']==loxP_type]
    bases=['A','T','C','G','insertions','deletions']
    for b in bases:
        df['{0} %'.format(b)]=(df[b]/df['reads_all'])*100

    df['reads_all']=df[['A','T','C','G']].sum(axis=1)
    df['top_base'] = df[['A','T','C','G']].max(axis=1)
    df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
    df['majority base %'] = (df['top_base'] / df['reads_all'])*100
    df['top_base_accuracy']=df.apply(accuracy,axis=1,args=('top_base_seq',))
    df['mut type']=df.apply(mut_type,axis=1,args=('top_base_seq',))

    df2=df[['pos','loxP type','majority base %', 'deletions %']]
    df2=pd.melt(df2,id_vars=['pos','loxP type'],value_vars=['majority base %','deletions %'])
    df2=df2.rename(columns={'variable':'base type','value':'Percent of reads in pileup'})
    dfd=df2[df2['base type']=='deletions %']
    dfd=dfd.rename(columns={'Percent of reads in pileup':'Percent of deletions in reads'})
    dfm=df2[df2['base type']=='majority base %']
    dfm=dfm.rename(columns={'Percent of reads in pileup':'Percent of majority base in reads'})

    df3=df[['pos','loxP type','top_base_accuracy']]
    df3=pd.melt(df3,id_vars=['pos','loxP type'],value_vars=['top_base_accuracy'])
    df3=df3.rename(columns={'variable':'base type','value':'Different to reference'})

    df4=df[['pos','reads_all','loxP type']]
    df4=df4.rename(columns={'reads_all':'Depth of coverage'})
    return df,df2,df3,df4,dfd,dfm

def setBackGrounds(r,ax):
    ax.axvspan(r['query start'],r['query end'], alpha=0.35)
    return ax

def plot(df,dfl,title,rb):
    df,df2,df3,df4,dfd,dfm=preDFs(df,'allLoxp')

    # subplots 
    nrows,ncols=3,1
    fig, ax = plt.subplots(nrows=nrows,ncols=ncols,
            gridspec_kw = {'height_ratios':[3,3,3]} )
                #'width_ratios':[5,2]})
    
    ax[2].get_shared_x_axes().join( ax[2],ax[1],ax[0] )
    # left hand plot
    g3=sns.lineplot(x='pos',y='Percent of deletions in reads',hue='loxP type',data=dfd,ax=ax[2],
		alpha=0.5)
    g2=sns.lineplot(x='pos',y='Percent of majority base in reads',hue='loxP type',data=dfm,ax=ax[1],
		alpha=0.5)
    g1=sns.lineplot(x='pos',y='Depth of coverage',data=df4,hue='loxP type',ax=ax[0],
		alpha=0.5)

    # loxP sites on background
    rb=rb[rb['alignment length'] > 45]
    for index, r in rb.iterrows():
        ax[0].text(r['query start'], 
		max(df4['Depth of coverage'])+40, 
		r['subject'],
		verticalalignment='top')

        for x in range(nrows):
            ax[x].axvspan(r['query start'],r['query end'], alpha=0.35)

    plt.suptitle(title.replace('_',' '))
    fig.set_size_inches(14.5, 8.5)
    plt.savefig('{0}_readmix.pdf'.format(title))

### bam manipulations

def roundup(x,n=50):
    return int(n * round(float(x)/n))

def binReads(blast_file,af):
    '''take blast file against raw reads and groupby hit numbers and types'''
    # read blast file remove short hits and duplicate hits with near similart start positions
    df=pd.read_csv(blast_file,sep='\t',names=names)
    df=df[df['alignment length'] > 45]
    df['fqstart']=df['query start'].map(roundup)
    idx=df.groupby(['query','fqstart'])['bit score'].transform(max) == df['bit score']
    df=df[idx]
    
    df['loxps'] = df.groupby('query')['query'].transform('count')
    df['loxp count'] = df.groupby(['query','subject'])['subject'].transform('count')
    
    # bin by number of 
    oneLoxp=df[df.loxps == 1]
    twoLoxp=df[df.loxps == 2]
    moreLoxp=df[df.loxps >= 3]

    oneLoxp5pr=oneLoxp[oneLoxp['subject']=='LoxP_5prime']
    oneLoxp3pr=oneLoxp[oneLoxp['subject']=='LoxP_3prime']
    twoLoxpHet=twoLoxp[twoLoxp['loxp count'] == 1]
    twoLoxpHom=twoLoxp[twoLoxp['loxp count'] == 2]
    #twoLoxpHom['query'].to_csv('twoLoxpHet.csv',index=False)
    #twoLoxpHet['query'].to_csv('twoLoxpHom.csv',index=False)
    d={'oneLoxp5pr':oneLoxp5pr['query'].tolist(),
        'oneLoxp3pr':oneLoxp3pr['query'].tolist(),
	'twoLoxpHet':twoLoxpHet['query'].tolist(),
	'twoLoxpHom':twoLoxpHom['query'].tolist(),
        'allLoxp':df['query'].tolist()}
    return(d)

def filterBam(ib,ob,l,include=True):
    '''take bam in put files, output file name and a list of seq ids not to include '''
    inBam=pysam.AlignmentFile(ib, "rb")
    outBam = pysam.AlignmentFile(ob, "wb", template=inBam)
    n=0
    for read in inBam.fetch():
        if include==True:
            if read.query_name in l:
                n+=1
                outBam.write(read)
        else:
            if read.query_name not in l:
                n+=1
                outBam.write(read)
    outBam.close()
    
    try:
        pysam.index(ob)
    except:
        print('Could not index {0} file'.format(ob))

    return len(l), n

def _pysamStats(b,r):
    mybam = pysam.AlignmentFile(b)
    for rec in pysamstats.stat_variation_strand(mybam,fafile=r):
        yield rec

def getPysamstats(b,r):
    ps=_pysamStats(b,r)
    df=pd.DataFrame(ps)
    return df

def run(opts):
    # manipulate bam file
    bins=binReads(opts.blast_file,opts.align_fraction)
    title=opts.sam_file.split('/')[-1].split('.')[0]
    pysamStats=[]
    stats=[]
    for b in bins:
        l,n=filterBam(opts.sam_file,'{0}_{1}.bam'.format(title,b), bins[b])
        d={'sample':title,'loxp mix':b,'reads':l,'mapped':n}
        stats.append(d)
        print(b,l,n)
        if n<10: continue
        df=getPysamstats('{0}_{1}.bam'.format(title,b),opts.ref_file)
        df['loxP type']=b
        pysamStats.append(df)

    l,n=filterBam(opts.sam_file,'{0}_noloxp.bam'.format(title), bins['allLoxp'],include=False)
    df=getPysamstats('{0}_noloxp.bam'.format(title),opts.ref_file)
    df['loxP type']='No loxP'
    pysamStats.append(df)
    print('No loxP',l,n)
    d={'sample':title,'loxp mix':'No loxP','reads':l,'mapped':n}
    stats_df=pd.DataFrame(stats)
    stats_df.to_csv('{0}_stats.csv'.format(title),index=False)
    stats.append(d)

    df=pd.concat(pysamStats)

    # plot data
    b=loadBlast(opts.blast_file)
    s=pd.DataFrame(loadBam(opts.sam_file))
    dfl=mergeBamBlast(b,s)
    rb=loadBlast(opts.ref_blast)
    plot(df,dfl,title,rb)
        
if __name__ == '__main__':
    parser = ArgumentParser(description='viz crispr regions from amplicon seq')
    parser.add_argument('-b','--blast_file', required=True,
		help='blast file of loxp or cre against reads')
    parser.add_argument('-s','--sam_file', required=True,
		help='sorted and indexed bam file of reads against ref file')
    parser.add_argument('-r','--ref_file', required=True,
		help='ref file')
    parser.add_argument('-rb','--ref_blast', required=True,
                help='blast of loxP against ref file')
    parser.add_argument('-af','--align_fraction', required=False,default=0.725,
                help='fraction of aligned determinant length required, default=0.725')
    opts, unknown_args = parser.parse_known_args()
    run(opts)
