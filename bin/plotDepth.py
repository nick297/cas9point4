#!/usr/bin/env python3
import matplotlib as mpl
mpl.use('Agg')
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import sys
sns.set_style('darkgrid')

def getData(f):
    names=['chrom','position','depth']
    df=pd.read_csv(f,sep='\t',names=names)
    return df

def plot(df):
    g=sns.lineplot('position','depth',data=df)
    title=str(sys.argv[1]).split('/')[-1].split('.')[0].replace('_',' ')
    plt.title(title)
    plt.savefig('{0}_depth.pdf'.format(title.replace(' ','_')))
    #plt.show()

df=getData(sys.argv[1])
plot(df)
