# Use an official Python runtime as a parent image
FROM ubuntu:16.04

# Set the working directory to /app
RUN mkdir /soft
WORKDIR /soft
# Install software
RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential


RUN apt-get -y install git python-pip python-biopython bwa python-dateutil python3-biopython python3-pip wget
RUN pip3 install pymongo tqdm ete3 pysam six scipy matplotlib seaborn pysam

## centrifuge
##RUN git clone https://github.com/infphilo/centrifuge
#WORKDIR /soft/centrifuge
#RUN pwd
#RUN make
#RUN make install prefix=/usr/local
#
## minimap
WORKDIR /soft
RUN git clone https://github.com/lh3/minimap2
WORKDIR /soft/minimap2
RUN apt-get install libz-dev
RUN make
RUN cp minimap2 /usr/local/bin/

#Porechop
WORKDIR /soft
RUN git clone https://github.com/nick297/Porechop.git
WORKDIR /soft/Porechop
RUN python3 setup.py install

# racon
RUN apt-get install -y cmake
WORKDIR /soft
RUN git clone --recursive https://github.com/isovic/racon.git racon
WORKDIR /soft/racon
RUN mkdir build
WORKDIR  /soft/racon/build
RUN cmake -DCMAKE_BUILD_TYPE=Release ..
RUN make install

# java
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get -y install default-jre-headless

# bowtie2
RUN apt-get install -y unzip
WORKDIR /soft
RUN wget https://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.3.4.1/bowtie2-2.3.4.1-linux-x86_64.zip
RUN unzip bowtie2-2.3.4.1-linux-x86_64.zip
WORKDIR /soft/bowtie2-2.3.4.1-linux-x86_64
ENV PATH="/soft/bowtie2-2.3.4.1-linux-x86_64:${PATH}"

# htslib
RUN apt-get install -y autoconf libbz2-dev  zlib1g-dev
WORKDIR /soft
RUN git clone https://github.com/samtools/htslib.git
WORKDIR htslib
RUN autoheader     # If using configure, generate the header template...
RUN autoconf       # ...and configure script (or use autoreconf to do both)
RUN ./configure  --disable-lzma  # Optional, needed for choosing optional functionality
RUN make
RUN make install

#samtools git
RUN apt-get install -y libncurses5-dev
WORKDIR /soft
RUN git clone https://github.com/samtools/samtools.git
WORKDIR /soft/samtools
RUN autoheader
RUN autoconf -Wno-syntax
RUN ./configure
RUN make
RUN make install

# blast+
WORKDIR /soft
RUN wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.7.1/ncbi-blast-2.7.1+-x64-linux.tar.gz
RUN tar zxvpf ncbi-blast-2.7.1+-x64-linux.tar.gz
ENV PATH="/soft/ncbi-blast-2.7.1+/bin:${PATH}"

# filtlong
WORKDIR /soft
RUN git clone https://github.com/rrwick/Filtlong.git
WORKDIR /soft/Filtlong
RUN make -j
RUN cp bin/filtlong /usr/local/bin

# bedtools
RUN apt-get install bedtools

# albacore
WORKDIR /soft
RUN wget https://mirror.oxfordnanoportal.com/software/analysis/ont_albacore-2.3.3-cp35-cp35m-manylinux1_x86_64.whl
RUN pip3 install ont_albacore-2.3.3-cp35-cp35m-manylinux1_x86_64.whl

#nanopolish 
WORKDIR /soft
RUN git clone --recursive https://github.com/jts/nanopolish.git
WORKDIR /soft/nanopolish
RUN make
