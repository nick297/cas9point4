# cas9point4

Analysis workflow for ONT data from mouse CRISPR amplicon sequencing 

## Dependencies

-nextflow

-albacore or guppy (optional)

-porechop (optional)

-filtlong

-minimap2

-samtools

-nanopolish

## Installation
Assuming all the dependencies are installed, the workflow can be installed by pulling from the gitlab page. There is also the option to run with conda using the -conda flag at runtime.

```
git clone https://gitlab.com/nick297/cas9point4.git
```

### Docker

### Singularity

## Run 
To run, a comma seperated (.csv) determinants file is required that lists the barcodes and refence sequences for mapping. Each barcode can be assigned multiple reference sequences for which it will be aligned to.

Here is an example of the csv file:

|  sample name                    | barcode | Number of determinants | name of determinant 1 | Length of determinant 1 | name of determinant 2 | Length of determinant 2 | name of determinant 3 | Length of determinant 3 | length % | Mutant ref name  |
|---------------------------------|---------|------------------------|-----------------------|-------------------------|-----------------------|-------------------------|-----------------------|-------------------------|----------|------------------|
|  MPEG1-CRE-CAS-LINE4-B6N/1.1c   | BC01    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  MPEG1-CRE-CAS-LINE3-B6N/1.1d   | BC02    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  PRO/4274.1h                    | BC03    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  PRO/4274.4a                    | BC04    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  CX3CL1-FLOX-CAS-LINE1-B6N/1.1c | BC05    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Cx3cl1\_Flox.fas  |
|  PRO/3976.1f                    | BC06    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Cx3cl1\_Flox.fas  |
|  PRO/4264.4a                    | BC07    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Pam\_Flox.fas     |
|  PAM-FLOX-CAS-LINE1-B6N/1.1a    | BC08    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Pam\_Flox.fas     |
|  PRO/4345.5a                    | BC09    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Prdm8\_Flox.fas   |
|  PRO/4345.3g                    | BC10    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Prdm8\_Flox.fas   |
|  PRO/4282.4e                    | BC11    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Hnf1a\_Flox.fas   |
|  PRO/4405.4a                    | BC12    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Inpp5k\_Flox.fas  |



This file can be called 'determinants_template.csv' and it will be detected automatically, or can be specified with the --determinantsFile option.

The location of the fast5 or fastq files is required with the --inFiles flag.

The location (directory of) of the reference sequence(s), correspoding to the determinants_file.csv meta data, must be specified with the --refDir option.

The location (file) of the determinants sequences (multi fasta file), corresponding to the determinants_file.csv meta data, must be specified with the --determinantsFasta option.

If you have downloaded this git repo, you can run the workflow like such:
```
# With basecalling (guppy (default) or albacore) and demultiplexing
nextflow run /location/of/cas9point4/main.nf \
        --inFiles /location/of/fast5_files \
        --inExp exp3 \
        --refDir /location/of/soft/cas9point4/refs/ \
        --determinantsFasta /location/of/cas9point4/transgene_sequences/loxp_cre.fa \
        -profile conda 

# With already basecalled fastq fils
nextflow run  /location/of/cas9point4/main.nf \
	--basecall false \
        --inFiles /location/of/fastq_files \
        --inExp exp3 \
        --refDir ~/cas9point4/refs/ \
        --determinantsFasta ~/cas9point4/transgene_sequences/loxp_cre.fa \
        -profile conda 

# With already basecalled and demultiplexed fastq files
nextflow run  /location/of/cas9point4/main.nf \
	--basecall false \
	--porechop false \
    --inFiles /location/of/fastq_files \
    --inExp exp3 \
    --refDir /location/of/cas9point4/refs/ \
    --determinantsFasta /location/of/cas9point4/transgene_sequences/loxp_cre.fa \
    -profile conda 


```

## Outputs
The workflow will output a number of folders containing files generated. These include:

'all': Bam files before filtering, containing all the reads


'basecalled': basecalled fastq files if using a basecaller


'covBases': plots of coverage proportion by majority base, insertions and deletions


'depthCharts': plots of coverage depth


'filtLong': fastq file output from filtong


'passBams': bam files only containing reads that pass the determinants file criteria


'passFailPlots': plot of read depth coverage for reads that passed or failed the determinants file criteria


'porechopFastqs': output fastq files from porechop if used


'psyamStats': output text file from pysamstats


'stats': stats txt files from samtools depths and idxstats



