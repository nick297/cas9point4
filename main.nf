params.basecall=true
params.porechop=true
params.qual=90
params.align_fraction = 0.725
quals= Channel.from( params.qual )
inExp = params.inExp
inFiles = params.inFiles
params.refDir="${params}.baseDir" + 'refs/'
params.basecaller='guppy'
basecaller=params.basecaller
params.determinantsCsv='determinants_template.csv'
determinants=file(params.determinantsCsv)
dFasta=file(params.determinantsFasta)
determinantsFasta=Channel.from( dFasta )

Channel
        .from( determinants )
        .splitCsv()
        .map { row -> tuple(row[1],row[10]) }
        .view()
        .set{ metaData }


if (params.basecall == true) {
process basecall {
	tag { run }

	publishDir 'basecalled', mode: 'link'

	cpus 4	

	input:
	//set val(run), val(fast5s) from ginputs1
        val(run) from inExp
	val(fast5s) from inFiles

	output:
	set val(run), val(fast5s), file(fastqs) into basecalled,basecalled2

	script:
	if( basecaller == 'albacore' )
		"""
		read_fast5_basecaller.py --barcoding -i $fast5s -t ${task.cpus} -s fastqs -f FLO-MIN107 -k SQK-LSK108 -r
		"""
	else if( basecaller == 'guppy' )
		"""
		guppy_basecaller -i $fast5s --device cuda:0 -c /opt/ont/guppy/data/dna_r9.4.1_450bps_fast.cfg -s fastqs -r
		"""
}}



if (params.porechop == true) {
if (params.basecall == true) {
process porechop {
	tag { run }
        //conda 'bioconda::porechop'

	publishDir 'porechopFastqs'

	cpus 4

        input: 
	set val(run), val(fast5s), file(fastqs) from basecalled
            
	output:
	file("porechop_fastqs/*") into porechopped


	script:
	"""
	porechop -i fastqs -b porechop_fastqs -t ${task.cpus}
	"""
}}
else if(params.basecall == false) {
process porechopnb {
	tag { run }
        //conda 'bioconda::porechop'

	publishDir 'porechopFastqs'

	cpus 4

	input:
        val(run) from inExp
        val(fastqs) from inFiles

	output:
	file("porechop_fastqs/*") into porechopped


	script:
	"""
	porechop -i $fastqs -b porechop_fastqs -t ${task.cpus}
	"""
}
}}

if (params.porechop == true) {
	demultiplexed=porechopped.flatten()
} else {
	demultiplexed = Channel
			.fromPath( params.inFiles + '/*.fastq' )
			.flatten()
}

process filtLong {
        tag { bc + ' ' + q }
        //conda 'bioconda::filtlong'
	publishDir 'filtLong'
	cpus 1

	input:
	set file(fastq),val(q) from demultiplexed.combine(quals)

	output:	
	set val(bc), file("${bc}_q${q}.fastq.gz"),val(q) into filtLonged

	script:
        bc = fastq.baseName.replace(".fastq","")
	"""
	filtlong --min_mean_q $q  ${bc}.fastq | gzip > ${bc}_q${q}.fastq.gz
	"""
}

process makeBlastDB {
	input:
	file('determinants.fa') from determinantsFasta
	
	output:
	set file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') into blastdb,blastdb2

	script:
	"""
	makeblastdb -in determinants.fa -dbtype nucl
	"""

}

process preRef {
	tag { bc + ' ' + d + ' ' + ref}
        //conda 'bioconda::blast'

        input:
        set val(bc), file("${bc}_${q}.fastq.gz"),val(q), val(ref),
	file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') from filtLonged.combine(metaData, by:0).combine(blastdb)

        output:
        set val(bc), file("${bc}_${q}.fastq.gz"),val(q),val(ref),file("*.fas"),file("${ref}.blast") into refs

        script:
        d=params.refDir
        """
        echo
        cp $d/${ref} ./
        blastn -query ${ref} -db determinants.fa -outfmt 6 -out ${ref}.blast 
        """
}

process map {
        tag { bc + '_' + r}
        //conda 'bioconda::minimap2 bioconda::samtools bioconda::pysam'
	cpus 4

	publishDir 'all', mode: 'copy', overwrite: true

	input:
        set val(bc), file('fastq'),val(q),val(r),file('ref'),file("${r}.blast") from refs


	output:
        set val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file('fastq'),file('ref'),file("${r}.blast"),val(q) into assembled1,assembled2

	script:
        exp=params.inExp
	"""
	minimap2 -ax map-ont $ref fastq  |\
	samtools view -bS -q 50 - | samtools sort -o out.bam 

        samtools index out.bam
        filterBam.py out.bam ${exp}_${bc}_${r}_q${q}.sorted.bam 0.50 0.3
	samtools index ${exp}_${bc}_${r}_q${q}.sorted.bam
        """
}

process bamStats {
	tag { bc + '_' + r}
        //conda 'bioconda::samtools pandas seaborn matplotlib pysamstats'

	publishDir 'stats', mode: 'copy', overwrite: true, pattern: '*.tsv'
        publishDir 'depthCharts', mode: 'copy', overwrite: true, pattern: '*depth.pdf'
        publishDir 'covBases', mode: 'copy', overwrite: true, pattern: '*covbases.pdf'
        publishDir 'psyamStats', mode: 'copy', overwrite: true, pattern: '*.pysam'
        

	input:
	set val(bc), val(r), val(exp), file('sorted.bam'), file('sorted.bam.bai'),file('fastq'), file(ref),file('blast'),val(q) from assembled1

	output:
        file("${exp}_${bc}_${r}_${q}.idxstats.tsv") into bstats
        set val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_${q}.depth.tsv") into depths
        file("*.pdf") into depthCharts
        file("*.pysam") into pysamStats

	script:
	"""
	samtools idxstats sorted.bam > ${exp}_${bc}_${r}_${q}.idxstats.tsv
        samtools depth -aa sorted.bam > ${exp}_${bc}_${r}_${q}.depth.tsv
        plotDepth.py ${exp}_${bc}_${r}_${q}.depth.tsv
	pysamstats -t variation_strand -f ref -d sorted.bam > ${exp}_${bc}_${r}_${q}.pysam
        covBases.py ${exp}_${bc}_${r}_${q}.pysam ${exp}_${bc}_${r}_${q}

	"""
}

process blastRawReads {
	tag { bc + '_' + r}
        //conda 'bioconda::blast'

	input:
	set val(bc), val(r), val(exp), file('sorted.bam'), file('sorted.bam.bai'),
		file('fastq'), file(ref),file("${r}.blast"),val(q),
		file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') from assembled2.combine(blastdb2)

	output:
        set val(r),val(bc),val(exp),file('sorted.bam'), file('sorted.bam.bai'), file("${bc}.blast"), file('ref'), file("${r}.blast")  into rblast

        script:
        """
	zcat <  fastq | sed '/^@/!d;s//>/;N' > fasta
        blastn -query fasta -db determinants.fa -outfmt 6 -out ${bc}.blast 
        """

}


process filterByDeterminants {
        errorStrategy 'ignore'
	tag { bc + '_' + r }
        //conda 'bioconda::samtools pandas seaborn matplotlib pysamstats pysam'

        publishDir 'passFailPlots', mode: 'copy', overwrite: true, pattern: '*.pdf'
        publishDir 'passBams', mode: 'copy', overwrite: true, pattern: '*passed.bam'
        publishDir 'passBams', mode: 'copy', overwrite: true, pattern: '*passed.bam.bai'

	input:
        set val(r),val(bc),val(exp),file("${exp}_${bc}_${r}_sorted.bam"), file("${exp}_${bc}_${r}_sorted.bam.bai"), file("${bc}.blast"), file('ref'), file("${r}.blast") from rblast

	output:
        file("*.pdf") optional true into loxPmixs
        file("*passed.bam*") optional true into loxPmixBams
	
	script:
	d=file(params.determinantsCsv)
	"""
	crisont.py -b "${bc}.blast" -s \
			"${exp}_${bc}_${r}_sorted.bam" \
			-r ref -rb ${r}.blast \
			-d $d \
			--barcode ${bc}

	"""

}

